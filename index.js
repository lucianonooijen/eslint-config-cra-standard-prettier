
module.exports = {
  extends: [
    'react-app',
    'standard',
    'standard-react',
    'standard-jsx',
    'plugin:prettier/recommended',
    'plugin:security/recommended',
    '@commonground/eslint-config/rules/generic',
    '@commonground/eslint-config/rules/prettier',
    '@commonground/eslint-config/rules/header',
    '@commonground/eslint-config/rules/react',
    '@commonground/eslint-config/rules/import',
    '@commonground/eslint-config/rules/jest',
    '@commonground/eslint-config/rules/security',
  ],

  plugins: [
    'header',
    'jest',
  ],
}
