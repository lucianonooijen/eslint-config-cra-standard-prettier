test('export does not throw', () => {
  expect(() => require('./index')).not.toThrow()
})
