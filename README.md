# eslint-config-cra-standard-prettier

Commonground shared eslint config for all our React projects. Designed to be used with create-react-app.

> Not using Create React App? See [our the Common Ground base ESLint rules](https://gitlab.com/commonground/core/eslint-config)

## Usage

Due to an [eslint config limitation](https://github.com/eslint/rfcs/pull/9) you have to install all dependencies that this config uses manually:

```shell
npm install --save-dev --save-exact \
@commonground/eslint-config-cra-standard-prettier \
eslint@7.x \
eslint-config-prettier@8.x \
eslint-config-react@1.x \
eslint-config-standard@16.x \
eslint-config-standard-jsx@10.x \
eslint-config-standard-react@11.x \
eslint-plugin-flowtype@5.x \
eslint-plugin-header@3.x \
eslint-plugin-import@2.x \
eslint-plugin-jest@24.x \
eslint-plugin-node@11.x \
eslint-plugin-prettier@3.x \
eslint-plugin-promise@4.x \
eslint-plugin-react@7.x \
eslint-plugin-security@1.x \
eslint-plugin-standard@4.x \
prettier@2.x \
react-scripts@3.x
```

Then override the `eslintConfig` setting in `package.json`:

```json
"eslintConfig": {
  "extends": "@commonground/eslint-config-cra-standard-prettier"
}
```

Alternatively, you can remove the `eslintConfig` entry and create a `.eslintrc.js` file, with:

```js
module.exports = {
  extends: ['@commonground/eslint-config-cra-standard-prettier']
}
```

This last method also opens the possibility to extend the rules, but in order to maintain a generic developer experience in all our projects, we recommend against this.

## Editor integration

### VSCode

Add the following extension: 

Then create the following file in the frontend project directory: `.vscode/settings.json` (it's .gitignored) containing:

```json
{
  "editor.defaultFormatter": "dbaeumer.vscode-eslint",
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  }
}
```

You may define this file at a higher level as well, but then make sure you add this to the settings.json:

```json
{
  "eslint.workingDirectories": [
    "./ui-directory"
  ]
}
```

## How to contribute

See [CONTRIBUTING](https://gitlab.com/commonground/core/eslint-config-cra-standard-prettier/-/blob/master/CONTRIBUTING.md)
